SEDEMKA

Sedemka je igra s kartami za 2 igralca. Igra se s kartami ve�jimi od 7, to�ke pa prina�ata le 
10 in as. Cilj igre je pridobiti najve� to�k. Igra se zaklju�i, ko se razdelijo vse karte, 
ali ko eden izmed igralcev dose�e dolo�eno �tevilo delnih zmag (�e ima navje� to�k ob razdelitvi 
vseh kart).

Potek igre:
Na za�etku oba igralca dobita po 4 karte. Prvi igralec vr�e poljubno karto, drugi lahko vr�e poljubno, ali isto.
�e vr�e isto, prvi igralec lahko prepusti rundo njemu, ali vr�e sedemko ali isto karto, �e ima.
Tisti, ki zmaga rundo, pobere karte na mizi. Na koncu se pogledajo no�ene karte vseh igralcev in tisti, ki ima ve� vrednih kart zmaga.
 