# -*- encoding: utf-8 -*-
from tkinter import *
import random
import time


vrednosti_kart=[7,8,9,10,"a","j","q","k"]
barve=["c","d","h","s"]

 

class Karta():
    def __init__(self,vrednost,vrsta):
        self.vrednost=vrednost
        self.vrsta=vrsta

    def __str__(self):
        return self.vrsta+"_"+str(self.vrednost)

 

class Igralec1():
    def __init__(self,roka=[],tocke=0,noseno=[]):
        self.roka=roka
        self.tocke=tocke


class Igralec2():
    def __init__(self,roka=[],tocke=0,noseno=[]):
        self.roka=roka
        self.tocke=tocke


class Miza():
    def __init__(self,karta=[]):
        self.karta=karta
    
    
class Igra:
    
    def __init__(self,master):

        self.platno=Canvas(master,relief="solid",bd=0,highlightthickness=0,width=800,height=800)
        self.platno.grid(row=2,rowspan=3,column=1,columnspan=3)

        self.gumb=Button(master,text="Začni novo igro",command=self.zacni,relief="solid",bd=2)
        self.gumb.grid(row=1,column=2)

        self.gumb2=Button(master,text="Zaključi rundo",command=self.zakljuci,relief="solid",bd=2,state=DISABLED)
        self.gumb2.grid(row=5,column=2)
        
        
    
        
        rwidth=self.platno.winfo_width()
        X=int((rwidth)/2)-100
        self.miza=Miza()
        self.igralec1=Igralec1()
        self.igralec2=Igralec2()

        

    def klik(self,event):
        height=self.platno.winfo_height()
        Y=int(height)
        rwidth=self.platno.winfo_width()
        X=int((rwidth)/2)-100
        x=self.platno.canvasx(event.x)
        y=self.platno.canvasy(event.y)
        m=len(self.tags)
        self.platno.delete("c1")
        self.platno.delete("c2")

        if self.na_potezi==True:
            for i in range(4):
                if self.platno.gettags(self.platno.find_enclosed(80*i+X-50, Y-150, 80*i+X+50, Y-50))==[]:
                    continue
                else:
                    if x>=80*i+X-50 and x <=80*i+X+20 and y<=Y-50 and y>=Y-150:
                      tag=self.platno.gettags(self.platno.find_enclosed(80*i+X-50, Y-150, 80*i+X+50, Y-50))[0]#dobimo katera slika je v danem kvadratu
                      self.platno.tag_raise(tag)
                      
                        
                      self.platno.move(tag, X+160-(80*i+X) +10*self.st,-Y+100+300+ 15*self.st)
                      self.mizaTags.append(tag)
                      
                      self.miza.karta.append(self.igralec2.roka[i])
                      
                      self.igralec2.roka[i]=None
                    
                      
                      self.na_potezi=False
                      for k in self.tags:
                        self.platno.itemconfigure(k,state=NORMAL)
                      self.st+=1
                      self.preveri1()
                      self.zadnji=self.igralec2


    def tocke(self):
        height=self.platno.winfo_height()
        Y=int(height)
        rwidth=self.platno.winfo_width()
        X=int(rwidth)
        self.prviTocke=0
        self.drugiTocke=0

        self.brisi_platno()
        
        for i in self.igralec1.noseno:
            if i=="a" or i==10:
                self.prviTocke+=10
        for j in self.igralec2.noseno:
            if j=="a" or j==10:
                self.drugiTocke+=10
        if self.zadnji==self.igralec1:
            self.prviTocke+=10
        else:
            self.drugiTocke+=10


        if self.prviTocke>self.drugiTocke:
           self.platno.create_text(X/2,Y/2-100,text="Zmagovalec= RACUNALNIK",font=("Times","28","bold"),fill="green")
##           self.platno.create_text(X/2,Y/2-100,text="Racunalnik je osvojil:"+str(self.prviTocke),font=("Times","18","bold"),fill="green")
        else:     
           self.platno.create_text(X/2,Y/2-100,text="Zmagovalec= JAZ",font=("Times","28","bold"),fill="green") 
##           self.platno.create_text(X/2,Y/2-100,text="Jaz sem osvojil:"+str(self.drugiTocke),font=("Times","28","bold"),fill="green")
   
   
    
        
        
    def brisi_platno(self):
        self.platno.delete("all")
        
    def brisi(self):
        for t in self.mizaTags:
                    self.platno.delete(t)
        self.mizaTags=[]

        
    def zakljuci(self):
        for k in self.tags:
            self.platno.itemconfigure(k,state=NORMAL)
        self.gumb2["state"]=DISABLED        
        self.kdo_nosi(self.igralec1.roka,self.igralec2.roka,self.miza.karta)
        
    def podaljsaj(self):
        height=self.platno.winfo_height()
        Y=int(height)
        rwidth=self.platno.winfo_width()
        X=int((rwidth)/2)-100
        
        self.enakiTags=[]
        self.na_potezi=False

        if self.miza.karta[-1].vrednost!=self.miza.karta[0].vrednost and self.miza.karta[-1].vrednost!=7 :
            self.kdo_nosi(self.prvi.noseno,self.drugi.noseno,self.miza.karta)


        else:
            a=self.seznam_vrednosti(self.igralec2.roka)
            b=self.seznam_vrednosti(self.miza.karta)
            self.preglej(b,a)
            if self.ind!=[]:
                for j in self.ind:
                    if self.platno.gettags(self.platno.find_enclosed(80*j+X-50, Y-150, 80*j+X+50, Y-50))!=[]:
                        tag=self.platno.gettags(self.platno.find_enclosed(80*j+X-50, Y-150, 80*j+X+50, Y-50))[0]
                        self.enakiTags.append(tag)
                    else:continue
                for k in self.tags:
                    if k not in self.enakiTags:
                       self.platno.itemconfigure(k,state=DISABLED)
            
                self.gumb2["state"]=NORMAL
                self.na_potezi=True

            else:
                self.kdo_nosi(self.prvi.noseno,self.drugi.noseno,self.miza.karta) 
        
            
            
            
    
    def izpisi1(self):
        height=self.platno.winfo_height()
        Y=int(height)
        rwidth=self.platno.winfo_width()
        X=int(rwidth)

        self.platno.delete("c1")
        self.platno.delete("c2")
        if self.prvi==self.igralec2:
           self.platno.create_text(X/2,170,text="NOSI: JAZ",font=("Times","14","bold"),fill="black",tags="c1")
        
        else:
            self.platno.create_text(X/2,170,text="NOSI: RACUNALNIK",font=("Times","14","bold"),fill="black",tags="c1")
        
            
    def izpisi2(self):
        height=self.platno.winfo_height()
        Y=int(height)
        rwidth=self.platno.winfo_width()
        X=int(rwidth)
        
        self.platno.delete("c1")
        self.platno.delete("c2")
        if self.drugi==self.igralec2:
            self.platno.create_text(X/2,170,text="NOSI: JAZ",font=("Times","14","bold"),fill="black",tags="c2")
        else:
            self.platno.create_text(X/2,170,text="NOSI: RACUNALNIK",font=("Times","14","bold"),fill="black",tags="c2")
        
    
    def kdo_igra(self):
        height=self.platno.winfo_height()
        Y=int(height)
        
        if self.prvi==self.igralec1:
            self.platno.create_text(50,Y/2,text="Na potezi je: RACUNALNIK",font=("Times","9","bold"),fill="black")  
            self.na_potezi=False
            self.platno.after(2100,self.racunalnik_igra)
                
        else:
            self.platno.create_text(60,Y/2,text="Na potezi je: JAZ",font=("Times","9","bold"),fill="black")     
            self.na_potezi=True

    def kateriNone(self):
        self.prviind=[]
        self.drugiind=[]
        for i in range(4):
            if self.igralec1.roka[i]==None:
                self.prviind.append(i)
        for i in range(4):
            if self.igralec2.roka[i]==None:
                self.drugiind.append(i)

    def deli(self,sez1,sez2):
        height=self.platno.winfo_height()
        Y=int(height)
        rwidth=self.platno.winfo_width()
        X=int((rwidth)/2)-100
        

        self.kateriNone()
        
        for i in range(len(self.prviind)):
            koncni=[None,None,None,None]
            if self.seznamKart!=[]:
               
                    sez2[self.prviind[i]]=self.seznamKart.pop()
                    sez1[self.drugiind[i]]=self.seznamKart.pop()
                    ime=str(sez1[self.drugiind[i]])
                    self.sl=PhotoImage(file= "./Slike/"+ime +".gif",width=100,height=100)         
                    self.karte.append(self.sl)

                    a=self.platno.create_image(80*self.drugiind[i]+X,Y-100,image=self.sl,tags="a"+str(len(self.tags)))           
                    self.platno.tag_bind(a,"<Button-1>",self.klik)
                    self.tags.append("a"+str(len(self.tags)))   
                    
                    
            elif self.seznamKart==[] and self.igralec1.roka==koncni and self.igralec2.roka==koncni:
                self.tocke()
           
            
        for i in range(4):
            if self.seznamKart!=[]:
                if sez2[i]==None:
                    sez2[i]=self.seznamKart.pop(-2)
                else:
                    continue

            
                
    def kdo_nosi(self,sez1,sez2,miza):
        print(len(self.miza.karta))
        if miza[-1].vrednost==miza[0].vrednost or miza[-1].vrednost==7:
            for i in range(len(miza)):
                sez2.append(miza[i]) 
            self.izpisi2()
            self.prvi,self.drugi=self.drugi,self.prvi
           
        else:
            for i in range(len(miza)):
                sez1.append(miza[i])
            self.izpisi1()

        koncni=[None,None,None,None]
        if  self.seznamKart==[] and self.igralec1.roka==koncni and self.igralec2.roka==koncni:
                self.platno.after(2100,self.tocke)
        else:

            self.deli(self.igralec2.roka,self.igralec1.roka)
            self.platno.after(2000,self.brisi)        
            self.st=0
            self.st1=0
            self.miza.karta=[]
            self.platno.after(2100,self.kdo_igra)
        print(len(self.seznamKart),self.igralec2.noseno,self.igralec1.roka,self.igralec2.roka)

    def preveri1(self):
        if self.prvi==self.igralec2:
            self.na_potezi=False
            self.platno.after(1000,self.racunalnik_igra)
            

        else:
            koncni=[None,None,None,None]
            if  self.seznamKart==[] and self.igralec1.roka==koncni and self.igralec2.roka==koncni:
                self.tocke()
            else:
                
                a=self.seznam_vrednosti(self.miza.karta)
                b=self.seznam_vrednosti(self.igralec1.roka)
                self.preglej(a,b)
                if self.enaka!=[] or self.sedemka!=[]:
                    self.racunalnik_daljsa()
                else:
                    self.kdo_nosi(self.prvi.noseno,self.drugi.noseno,self.miza.karta)
                    self.miza.karta=[]
                    self.kdo_igra()
           
    def preveri2(self):
        self.na_potezi=False
        if self.prvi==self.igralec1:
            self.na_potezi=True
            

        else: 
            koncni=[None,None,None,None]
            if  self.seznamKart==[] and self.igralec1.roka==koncni and self.igralec2.roka==koncni:
                self.tocke()

            else:
                self.podaljsaj()
            
    def seznam_vrednosti(self,seznam):         
        self.seznamVrednosti=[]
        for l in range(len(seznam)):
            if seznam[l]!=None:
                self.seznamVrednosti.append(seznam[l].vrednost)
            else:
                self.seznamVrednosti.append(0)
        return self.seznamVrednosti    
    
    def zacni(self):
        self.na_potezi=True
        self.prvi=self.igralec2
        self.drugi=self.igralec1
        self.mizaTags=[] # da bom kasneje lahko počistila mizo
        self.tags2=[] #id-ji od igralca1

        self.miza.karta=[]
        self.zadnji=self.igralec1
        self.igralec2.roka=[]
        self.igralec1.roka=[]
        self.igralec2.noseno=[]
        self.igralec1.noseno=[]
        
        height=self.platno.winfo_height()
        Y=int(height)
        self.st=0
        self.st1=0
        self.tst=0
        self.karte=[]
        self.tags=[]
        self.platno.delete("all")
        self.seznamKart=[]

        for i in vrednosti_kart:
            for j in barve:
                self.seznamKart.append(Karta(i,j))
        random.shuffle(self.seznamKart)
        
        
        l=len(self.seznamKart)         
        for j in range(1,5):
            self.igralec1.roka.append(self.seznamKart.pop(l-2*j+1))
            self.igralec2.roka.append(self.seznamKart.pop(l-2*j))
        
        
        rwidth=self.platno.winfo_width() #širina canvasa
        X=int((rwidth)/2)-100            #za koliko premaknemo slike
            
                           
        self.platno.create_text(X+70,240,text="Racunalnik",font=("Times","12","bold"),fill="black")    
        self.platno.create_text(X+140,240,text="Jaz",font=("Times","12","bold"),fill="black")

        for k in range(4):
            ime=str(self.igralec2.roka[k])
            self.sl=PhotoImage(file= "./Slike/"+ime +".gif",width=100,height=100)         
            self.karte.append(self.sl)

            a=self.platno.create_image(80*k+X,Y-100,image=self.sl,tags="a"+str(k))
            self.platno.tag_bind("a"+str(k),"<Button-1>",self.klik)
            self.tags.append("a"+str(k))
       
        self.sli=PhotoImage(file="./Slike/nasprotnik.gif",width=100,height=100)
        self.platno.create_image(X,100,image=self.sli)
        self.platno.create_image(80+X,100,image=self.sli)
        self.platno.create_image(160+X,100,image=self.sli)
        self.platno.create_image(240+X,100,image=self.sli)     

        self.platno.config(scrollregion=self.platno.bbox(ALL))        




    def koliko_vrednihKart(self):
        self.stevec=0
        for i in self.seznam_vrednosti(self.miza.karta):
            if i==10 or i=="a":
                self.stevec+=1


    def racunalnik_daljsa(self):
        height=self.platno.winfo_height()
        Y=int(height)
        rwidth=self.platno.winfo_width()
        X=(int((rwidth))/2)-100
        
        a=self.seznam_vrednosti(self.igralec1.roka)
        b=self.seznam_vrednosti(self.miza.karta)
        self.preglej(b,a)
        
        if self.miza.karta[0]==7:
            self.kdo_nosi(self.prvi.noseno,self.drugi.noseno,self.miza.karta)
        elif self.miza.karta[-1]!=self.miza.karta[0]:
            self.kdo_nosi(self.prvi.noseno,self.drugi.noseno,self.miza.karta)
        elif (self.miza.karta[0]==10 or self.miza.karta[0]=="a") and self.enaka!=[]:
            ime=str(self.igralec1.roka[self.ind1[0]])
            self.miza.karta.append(self.igralec1.roka[self.ind1[0]])

            self.slikca=PhotoImage(file="./Slike/"+ime+".gif",width=100,height=100)
            self.karte.append(self.slikca)
                     
            c=self.platno.create_image(80+X-10*self.st1,300+15*self.st1,image=self.slikca,tags="b"+str(len(self.tags2)))
            self.tags2.append("b"+str(len(self.tags2)))
            self.mizaTags.append("b"+str(len(self.tags2)))
            self.igralec1.roka[self.ind1[0]]=None
            self.zadnji=self.igralec1
            self.na_potezi=True
        elif (self.miza.karta[0]==10 or self.miza.karta[0]=="a") and self.sedemka!=[] :
            ime=str(self.igralec1.roka[self.ind2[0]])
            self.miza.karta.append(self.igralec1.roka[self.ind2[0]])
            self.zadnji=self.igralec1
            

            self.slikca=PhotoImage(file="./Slike/"+ime+".gif",width=100,height=100)
            self.karte.append(self.slikca)
                     
            c=self.platno.create_image(80+X-10*self.st1,300+15*self.st1,image=self.slikca,tags="b"+str(len(self.tags2)))
            self.tags2.append("b"+str(len(self.tags2)))
            self.mizaTags.append("b"+str(len(self.tags2)))
            self.igralec1.roka[self.ind2[0]]=None

            self.na_potezi=True
            
        else:
            self.kdo_nosi(self.prvi.noseno,self.drugi.noseno,self.miza.karta)

    def dodaj(self,slov,k,l):
        if k not in slov:
            slov[k]=[l]
        else:
            slov[k].append(l)

        
    def vrednost_praznaMiza(self,i,slovar):
        self.vrednost=0
        b=self.igralec1.roka[i].vrednost
        c=self.igralec1.roka[i]
        
        if self.prvi==self.igralec1:
            if b==7:
                self.vrednost=5
                self.dodaj(slovar,5,i)
            elif b==10 or b=="a" :
                self.vrednost=15
                self.dodaj(slovar,15,i)
            else:
                self.vrednost=10
                self.dodaj(slovar,10,i)
        else:
            prvaKarta=self.miza.karta[0].vrednost
            if prvaKarta==10 :
                if b==10:
                   self.vrednost=20
                   self.dodaj(slovar,20,i)
                elif b==7:
                   self.vrednost=15
                   self.dodaj(slovar,15,i)
                elif b=="a":
                   self.vrednost=5
                   self.dodaj(slovar,5,i)
                else:
                    self.vrednost=10
                    self.dodaj(slovar,10,i)
                    

            elif prvaKarta=="a":
                if b=="a":
                   self.vrednost=20
                   self.dodaj(slovar,20,i)
                elif b==7:
                   self.vrednost=15
                   self.dodaj(slovar,15,i)
                elif b==10:
                   self.vrednost=5
                   self.dodaj(slovar,5,i)
                else:
                    self.vrednost=10
                    self.dodaj(slovar,10,i)


            elif prvaKarta==7:
                if b==10 or b=="a":
                    self.vrednost=5
                    self.dodaj(slovar,5,i)
                elif b==7:
                    self.koliko_vrednihKart()
                    if self.stevec!=0:
                        self.vrednost=20
                        self.dodaj(slovar,20,i)
                    else:
                        self.vrednost=10
                        self.dodaj(slovar,10,i)
                else:
                    self.vrednost=12
                    self.dodaj(slovar,12,i)
                    

                    
                 
            else:
                if b==prvaKarta:
                    self.vrednost=20
                    self.dodaj(slovar,20,i)
                elif b==10 or b=="a":
                    self.vrednost=5
                    self.dodaj(slovar,5,i)
                elif b==7:
                    self.koliko_vrednihKart()
                    if self.stevec!=0:
                        self.vrednost=15
                        self.dodaj(slovar,15,i)
                    else:
                        self.vrednost=10
                        self.dodaj(slovar,10,i)
                else:
                    self.vrednost=12
                    self.dodaj(slovar,12,i)
                    


    def vrzi(self):
        self.Poteze={}
        self.index=None
        for i in range(len(self.igralec1.roka)):
            if self.igralec1.roka[i]!=None:
                self.vrednost_praznaMiza(i,self.Poteze)
            else:
                continue

     
        maks=max(self.Poteze)
        print(self.Poteze,maks)
        indeksi=self.Poteze[maks]
        self.index=random.choice(indeksi)          
        

        


    
    def preglej(self,sez1,sez2):
        
        self.enaka=[]
        self.sedemka=[]
        self.ind=[]
        self.ind1=[]
        self.ind2=[]
        prva=sez1[0]
        for i in range(len(sez2)):
            if sez2[i]==prva:
               self.enaka.append(sez2[i])
               self.ind.append(i)
               self.ind1.append(i)
            
            elif sez2[i]==7:
                self.sedemka.append(sez2[i])
                self.ind.append(i)
                self.ind2.append(i)

    def risi2(self,index):
        rwidth=self.platno.winfo_width()
        X=int((rwidth)/2)-100
        
        ime=str(self.igralec1.roka[index])
                
        self.miza.karta.append(self.igralec1.roka[index])
        
        
        self.slikca=PhotoImage(file="./Slike/"+ime+".gif",width=100,height=100)
        self.karte.append(self.slikca)
         
        c=self.platno.create_image(80+X-10*self.st1,300+15*self.st1,image=self.slikca,tags="b"+str(len(self.tags2)))
        self.tags2.append("b"+str(len(self.tags2)))
        self.mizaTags.append("b"+str(self.tst))

    
    def racunalnik_igra(self):
        self.platno.delete("c1")
        self.platno.delete("c2")
        self.gumb2["state"]=DISABLED
        if self.na_potezi==False:
            rwidth=self.platno.winfo_width()
            X=int((rwidth)/2)-100
            a=self.seznam_vrednosti(self.miza.karta)
            b=self.seznam_vrednosti(self.igralec1.roka)

            self.vrzi()
            self.risi2(self.index)
            
            self.igralec1.roka[self.index]=None
            self.zadnji=self.igralec1

            self.st1+=1
            self.tst+=1
            
            
            self.preveri2()
          
                 


root=Tk()
root.title("Sedemka")
root.option_add("*background","white")
aplikacija=Igra(root)
root.configure(background="white")
root.mainloop()


